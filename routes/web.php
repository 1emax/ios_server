<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [
    HomeController::class, 'index'
])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get(\App\Models\Users::LINK_PREFIX . '{link}', [\App\Http\Controllers\BookingsController::class, 'show']);
Route::get('/reset_success', function () {
    return 'Вы успешно обновили пароль!';
});
Route::post('/new_book', [\App\Http\Controllers\BookingsController::class, 'newBook'])->name('new_book');
Route::get('/free_time/{userId}/{date}/{service_ids?}', [\App\Http\Controllers\SchedulesController::class, 'userFreeTime']);
