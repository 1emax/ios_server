<?php namespace Tests\Repositories;

use App\Models\Bookings;
use App\Repositories\BookingsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class BookingsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var BookingsRepository
     */
    protected $bookingsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->bookingsRepo = \App::make(BookingsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_bookings()
    {
        $bookings = Bookings::factory()->make()->toArray();

        $createdBookings = $this->bookingsRepo->create($bookings);

        $createdBookings = $createdBookings->toArray();
        $this->assertArrayHasKey('id', $createdBookings);
        $this->assertNotNull($createdBookings['id'], 'Created Bookings must have id specified');
        $this->assertNotNull(Bookings::find($createdBookings['id']), 'Bookings with given id must be in DB');
        $this->assertModelData($bookings, $createdBookings);
    }

    /**
     * @test read
     */
    public function test_read_bookings()
    {
        $bookings = Bookings::factory()->create();

        $dbBookings = $this->bookingsRepo->find($bookings->id);

        $dbBookings = $dbBookings->toArray();
        $this->assertModelData($bookings->toArray(), $dbBookings);
    }

    /**
     * @test update
     */
    public function test_update_bookings()
    {
        $bookings = Bookings::factory()->create();
        $fakeBookings = Bookings::factory()->make()->toArray();

        $updatedBookings = $this->bookingsRepo->update($fakeBookings, $bookings->id);

        $this->assertModelData($fakeBookings, $updatedBookings->toArray());
        $dbBookings = $this->bookingsRepo->find($bookings->id);
        $this->assertModelData($fakeBookings, $dbBookings->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_bookings()
    {
        $bookings = Bookings::factory()->create();

        $resp = $this->bookingsRepo->delete($bookings->id);

        $this->assertTrue($resp);
        $this->assertNull(Bookings::find($bookings->id), 'Bookings should not exist in DB');
    }
}
