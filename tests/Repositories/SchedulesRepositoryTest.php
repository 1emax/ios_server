<?php namespace Tests\Repositories;

use App\Models\Schedules;
use App\Repositories\SchedulesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SchedulesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SchedulesRepository
     */
    protected $schedulesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->schedulesRepo = \App::make(SchedulesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_schedules()
    {
        $schedules = Schedules::factory()->make()->toArray();

        $createdSchedules = $this->schedulesRepo->create($schedules);

        $createdSchedules = $createdSchedules->toArray();
        $this->assertArrayHasKey('id', $createdSchedules);
        $this->assertNotNull($createdSchedules['id'], 'Created Schedules must have id specified');
        $this->assertNotNull(Schedules::find($createdSchedules['id']), 'Schedules with given id must be in DB');
        $this->assertModelData($schedules, $createdSchedules);
    }

    /**
     * @test read
     */
    public function test_read_schedules()
    {
        $schedules = Schedules::factory()->create();

        $dbSchedules = $this->schedulesRepo->find($schedules->id);

        $dbSchedules = $dbSchedules->toArray();
        $this->assertModelData($schedules->toArray(), $dbSchedules);
    }

    /**
     * @test update
     */
    public function test_update_schedules()
    {
        $schedules = Schedules::factory()->create();
        $fakeSchedules = Schedules::factory()->make()->toArray();

        $updatedSchedules = $this->schedulesRepo->update($fakeSchedules, $schedules->id);

        $this->assertModelData($fakeSchedules, $updatedSchedules->toArray());
        $dbSchedules = $this->schedulesRepo->find($schedules->id);
        $this->assertModelData($fakeSchedules, $dbSchedules->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_schedules()
    {
        $schedules = Schedules::factory()->create();

        $resp = $this->schedulesRepo->delete($schedules->id);

        $this->assertTrue($resp);
        $this->assertNull(Schedules::find($schedules->id), 'Schedules should not exist in DB');
    }
}
