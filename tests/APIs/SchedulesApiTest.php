<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Schedules;

class SchedulesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_schedules()
    {
        $schedules = Schedules::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/schedules', $schedules
        );

        $this->assertApiResponse($schedules);
    }

    /**
     * @test
     */
    public function test_read_schedules()
    {
        $schedules = Schedules::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/schedules/'.$schedules->id
        );

        $this->assertApiResponse($schedules->toArray());
    }

    /**
     * @test
     */
    public function test_update_schedules()
    {
        $schedules = Schedules::factory()->create();
        $editedSchedules = Schedules::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/schedules/'.$schedules->id,
            $editedSchedules
        );

        $this->assertApiResponse($editedSchedules);
    }

    /**
     * @test
     */
    public function test_delete_schedules()
    {
        $schedules = Schedules::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/schedules/'.$schedules->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/schedules/'.$schedules->id
        );

        $this->response->assertStatus(404);
    }
}
