<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Bookings;

class BookingsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_bookings()
    {
        $bookings = Bookings::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/bookings', $bookings
        );

        $this->assertApiResponse($bookings);
    }

    /**
     * @test
     */
    public function test_read_bookings()
    {
        $bookings = Bookings::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/bookings/'.$bookings->id
        );

        $this->assertApiResponse($bookings->toArray());
    }

    /**
     * @test
     */
    public function test_update_bookings()
    {
        $bookings = Bookings::factory()->create();
        $editedBookings = Bookings::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/bookings/'.$bookings->id,
            $editedBookings
        );

        $this->assertApiResponse($editedBookings);
    }

    /**
     * @test
     */
    public function test_delete_bookings()
    {
        $bookings = Bookings::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/bookings/'.$bookings->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/bookings/'.$bookings->id
        );

        $this->response->assertStatus(404);
    }
}
