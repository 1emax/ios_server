<?php

namespace App\Repositories;

use App\Models\Bookings;
use App\Repositories\BaseRepository;

/**
 * Class BookingsRepository
 * @package App\Repositories
*/

class BookingsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'phone',
        'user_id',
        'start_schedule_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bookings::class;
    }
}
