<?php

namespace App\Repositories;

use App\Models\Schedules;
use App\Repositories\BaseRepository;

/**
 * Class SchedulesRepository
 * @package App\Repositories
*/

class SchedulesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'datetime_from',
        'datetime_to',
        'user_id',
        'reserved',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Schedules::class;
    }
}
