<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Swagger\Annotations as SWG;

/**
 * Class Bookings
 * @package App\Models
 *
 * @property \App\Models\Schedules $startSchedule
 * @property \App\Models\Users $user
 * @property \Illuminate\Database\Eloquent\Collection $services
 * @property string $name
 * @property string $phone
 * @property integer $user_id
 * @property integer $start_schedule_id
 *
 * @SWG\Definition(
 *      definition="Bookings",
 *      required={"name", "phone", "user_id", "start_schedule_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="start_schedule_id",
 *          description="start_schedule_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Bookings extends Model
{

    use HasFactory;

    public $table = 'bookings';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'name',
        'phone',
        'user_id',
        'start_schedule_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'phone' => 'string',
        'user_id' => 'integer',
        'start_schedule_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:256',
        'phone' => 'required|string|max:128',
        'user_id' => 'required',
        'start_schedule_id' => 'required|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function startSchedule()
    {
        return $this->belongsTo(\App\Models\Schedules::class, 'start_schedule_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\Users::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function services()
    {
        return $this->belongsToMany(\App\Models\Services::class, 'bookings_services', 'booking_id', 'service_id');
    }
}
