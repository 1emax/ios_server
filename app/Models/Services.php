<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Swagger\Annotations as SWG;

/**
 * Class Services
 * @package App\Models
 *
 * @property \App\Models\Users $user
 * @property \Illuminate\Database\Eloquent\Collection $bookings
 * @property integer $user_id
 * @property string $name
 * @property integer $cost
 * @property integer $duration
 * @property integer $app_id
 *
 * @SWG\Definition(
 *      definition="Services",
 *      required={"user_id", "cost", "duration"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cost",
 *          description="cost",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="duration",
 *          description="duration",
 *          type="integer",
 *          format="int32"
 *      ),
 *     @SWG\Property(
 *          property="app_id",
 *          description="app_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Services extends Model
{

    use HasFactory;

    public $table = 'services';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $hidden = ['pivot'];


    public $fillable = [
        'user_id',
        'name',
        'cost',
        'duration',
        'app_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'name' => 'string',
        'cost' => 'integer',
        'duration' => 'integer',
        'app_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'name' => 'required|string|max:512',
        'cost' => 'required|integer',
        'duration' => 'required|integer',
        'app_id' => 'integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\Users::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function bookings()
    {
        return $this->belongsToMany(\App\Models\Bookings::class, 'bookings_services', 'service_id', 'booking_id');
    }
}
