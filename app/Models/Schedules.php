<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Swagger\Annotations as SWG;

/**
 * Class Schedules
 * @package App\Models
 *
 * @property Carbon $datetime_from
 * @property Carbon $datetime_to
 * @property integer $user_id
 * @property boolean $reserved
 *
 *
 * @SWG\Definition(
 *      definition="Schedules",
 *      required={"datetime_from", "datetime_to", "user_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="datetime_from",
 *          description="datetime_from",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="datetime_to",
 *          description="datetime_to",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="reserved",
 *          description="reserved",
 *          type="boolean"
 *      )
 * )
 */
class Schedules extends Model
{

    use HasFactory;

    public $table = 'schedules';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'datetime_from',
        'datetime_to',
        'user_id',
        'reserved',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'datetime_from' => 'datetime',
        'datetime_to' => 'datetime',
        'user_id' => 'integer',
        'reserved' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'datetime_from' => 'required|date_format:Y-m-d H:i:s',
        'datetime_to' => 'required|date_format:Y-m-d H:i:s|gt:datetime_from',
        'user_id' => 'required|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'reserved' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\Users::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function bookings()
    {
        return $this->hasMany(\App\Models\Bookings::class, 'start_schedule_id');
    }

    /**
     * @param integer $minutes
     */
    public function reserveAnyForNextMinutes($minutes)
    {
        $this->reserved = true;
        $this->save();


        self::where('user_id', $this->user_id)
            ->where('datetime_from', '>=', $this->datetime_from)
            ->where('datetime_from', '<', $this->datetime_from->addMinutes($minutes))
            ->update(['reserved' => true]);
    }
}
