<?php

namespace App\Libraries;


use App\Models\Bookings;
use App\Models\Users;

class IOSPush
{
    /** @var Users */
    private $user;
    /** @var Bookings */
    private $booking;

    public function __construct(Users $user, Bookings $booking)
    {
        $this->user = $user;
        $this->booking = $booking;
    }

    public function send()
    {
        // Provide the Host Information.
        $tHost = env('APNS_HOST');
        // $tHost = 'gateway.sandbox.push.apple.com'; //'gateway.push.apple.com';
        $tPort = env('APNS_PORT');
        // Provide the Certificate and Key Data.
        $tCert = dirname(dirname(__DIR__)) .'/' . env('CERTIFICATE_PATH');
        $tPassphrase = env('PASSPHRASE');
        $tToken = $this->user->device_token;

        if (!$tToken) {
            // todo: throw an exception
            return;
        }

        // The content that is returned by the LiveCode “pushNotificationReceived” message.
        $tPayload = 'APNS payload';
        // Create the message content that is to be sent to the device.
        // Create the payload body
        //Below code for non silent notification
        $tBody['aps'] = array(
            'badge' => 2, //+1,
            'alert' => [
                "title" => "Новая бронь",
                // "subtitle" => "Подзаголовок",
                "body" => 'Запись на ' . $this->booking->startSchedule->datetime_from->format('Y-m-d H:i'),
            ],
            'sound' => 'default'
        );
        $tBody ['payload'] = $tPayload;
        // Encode the body to JSON.
        $tBody = json_encode ($tBody);
        // Create the Socket Stream.
        $tContext = stream_context_create ();
        stream_context_set_option ($tContext, 'ssl', 'local_cert', $tCert);
        // Remove this line if you would like to enter the Private Key Passphrase manually.
        stream_context_set_option ($tContext, 'ssl', 'passphrase', $tPassphrase);
        // Open the Connection to the APNS Server.
        $tSocket = stream_socket_client ('ssl://'.$tHost.':'.$tPort, $error, $errstr, 30, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $tContext);
        // Check if we were able to open a socket.
        if (!$tSocket) {
            // todo: throw an exception
            return "APNS Connection Failed: $error $errstr" . PHP_EOL;
        }
        // Build the Binary Notification.
        $tMsg = chr (0) . chr (0) . chr (32) . pack ('H*', $tToken) . pack ('n', strlen ($tBody)) . $tBody;
        // Send the Notification to the Server.
        $tResult = fwrite ($tSocket, $tMsg, strlen ($tMsg));

        $response = $tResult ? 'Delivered Message to APNS' : 'Could not Deliver Message to APNS';

        // Close the Connection to the Server.
        fclose ($tSocket);

        return $response;
    }
}
