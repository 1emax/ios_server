<?php


namespace App\Http\Controllers;


use App\Libraries\IOSPush;
use App\Models\Bookings;
use App\Models\Users;
use App\Repositories\BookingsRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Expression;
use Illuminate\Http\Request;

class BookingsController extends Controller
{
    /** @var  BookingsRepository */
    private $bookingsRepository;

    public function __construct(BookingsRepository $bookingsRepo)
    {
        $this->bookingsRepository = $bookingsRepo;
    }

    public function show($link)
    {
        /** @var Bookings $bookings */
        $user = Users::where('link', '=', $link)
            ->with('services')
            ->with('schedules', function (HasMany $query) {
                $query
                    ->select(['user_id', new Expression('DATE(datetime_from) as schedule_date')])
                    ->where('datetime_to', '>' , Carbon::now())
                    ->where('reserved', false)
                    ->groupBy(new Expression('schedule_date'))
                ;
            })
            ->firstOrFail();

        return view('users.page', compact('user'));
    }

    public function newBook(Request $request)
    {
        // todo: could be saved twice or more. Fix it
        $input = $request->all();

        /** @var $booking Bookings */
        $booking = $this->bookingsRepository->create($input);
        $booking->services()->attach($input['service_ids']);

        $booking->startSchedule->reserveAnyForNextMinutes($booking->services->sum('duration'));

        if ($booking->wasRecentlyCreated) {
            (new IOSPush($booking->user, $booking))->send();
        }

        return view('bookings.thank-you', compact('booking'));
    }
}
