<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateServicesAPIRequest;
use App\Http\Requests\API\UpdateServicesAPIRequest;
use App\Models\Services;
use App\Repositories\ServicesRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Swagger\Annotations as SWG;

/**
 * Class ServicesController
 * @package App\Http\Controllers\API
 */

class ServicesAPIController extends AppBaseController
{
    /** @var  ServicesRepository */
    private $servicesRepository;

    public function __construct(ServicesRepository $servicesRepo)
    {
        $this->servicesRepository = $servicesRepo;
    }

    /**
     * Display a listing of the Services.
     * GET|HEAD /services
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/services",
     *      summary="Get a listing of the user Services.",
     *      tags={"Services"},
     *      description="Get user all Services",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="user_id",
     *          description="id of Services owner",
     *          type="integer",
     *          required=true,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Services")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $services = $this->servicesRepository->all(
            [
                'user_id' => $request->get('user_id'),
            ] + $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($services->toArray(), 'Services retrieved successfully');
    }

    /**
     * Store a newly created Services in storage.
     * POST /services
     *
     * @param CreateServicesAPIRequest $request
     * @return JsonResponse
     *
     * @SWG\Post(
     *      path="/services",
     *      summary="Store a newly created Services in storage",
     *      tags={"Services"},
     *      description="Store Services",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Services that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Services")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Services"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateServicesAPIRequest $request)
    {
        $input = $request->all();

        $services = $this->servicesRepository->create($input);

        return $this->sendResponse($services->toArray(), 'Services saved successfully');
    }

    /**
     * Display the specified Service.
     * GET|HEAD /services/{id}
     *
     * @param         $app_id
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/services/{app_id}",
     *      summary="Display the specified Services",
     *      tags={"Services"},
     *      description="Get Services",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="app_id",
     *          description="id of Service in ios app",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *     @SWG\Parameter(
     *          name="user_id",
     *          description="id of Services owner",
     *          type="integer",
     *          required=true,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Services"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($app_id, Request $request)
    {
        /** @var Services $services */
        $services = $this->servicesRepository->all([
            'user_id' => $request->get('user_id'),
            'app_id' => $app_id,
        ])->first();

        if (empty($services)) {
            return $this->sendError('Services not found');
        }

        return $this->sendResponse($services->toArray(), 'Services retrieved successfully');
    }

    /**
     * Update the specified Service in storage.
     * PUT/PATCH /services/{app_id}
     *
     * @param int $app_id
     * @param UpdateServicesAPIRequest $request
     * @return JsonResponse
     *
     * @SWG\Put(
     *      path="/services/{app_id}",
     *      summary="Update the specified Service in storage",
     *      tags={"Services"},
     *      description="Update Services",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="app_id",
     *          description="id of Service in ios app",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *     @SWG\Parameter(
     *          name="user_id",
     *          description="id of Services owner",
     *          type="integer",
     *          required=true,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Services that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Services")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Services"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($app_id, UpdateServicesAPIRequest $request)
    {
        $user_id = $request->get('user_id');

        $input = $request->input();

        /** @var Services $services */
        $services = $this->servicesRepository->all(compact('app_id', 'user_id'))->first();

        if (empty($services)) {
            return $this->sendError('Services not found');
        }

        $services = $this->servicesRepository->update($input, $services->id);

        return $this->sendResponse($services->toArray(), 'Services updated successfully');
    }

    /**
     * Remove the specified Services from storage.
     * DELETE /services/{id}
     *
     * @param         $app_id
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \Exception
     * @SWG\Delete(
     *      path="/services/{app_id}",
     *      summary="Remove the specified Services from storage",
     *      tags={"Services"},
     *      description="Delete Services",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="app_id",
     *          description="id of Service in ios app",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *     @SWG\Parameter(
     *          name="user_id",
     *          description="id of Services owner",
     *          type="integer",
     *          required=true,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($app_id, Request $request)
    {
        $user_id = $request->get('user_id');

        /** @var Services $services */
        $services = $this->servicesRepository->all(compact('app_id', 'user_id'))
            ->first();

        if (empty($services)) {
            return $this->sendError('Services not found');
        }

        $services->delete();

        return $this->sendSuccess('Services deleted successfully');
    }
}
