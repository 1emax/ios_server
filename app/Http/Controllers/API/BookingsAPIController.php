<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBookingsAPIRequest;
use App\Http\Requests\API\UpdateBookingsAPIRequest;
use App\Models\Bookings;
use App\Repositories\BookingsRepository;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Swagger\Annotations as SWG;

/**
 * Class BookingsController
 * @package App\Http\Controllers\API
 */

class BookingsAPIController extends AppBaseController
{
    /** @var  BookingsRepository */
    private $bookingsRepository;

    public function __construct(BookingsRepository $bookingsRepo)
    {
        $this->bookingsRepository = $bookingsRepo;
    }

    /**
     * Display a listing of the Bookings.
     * GET|HEAD /bookings
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/bookings",
     *      summary="Get a listing of the user Bookings.",
     *      tags={"Bookings"},
     *      description="Get user all Bookings",
     *      produces={"application/json"},
     *     @SWG\Parameter(
     *          name="user_id",
     *          description="id of Bookings owner",
     *          type="integer",
     *          required=true,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Bookings")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $bookings = $this->bookingsRepository->all(
            ['user_id' => $request->get('user_id')] + $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($bookings->toArray(), 'Bookings retrieved successfully');
    }

    /**
     * Display the specified Bookings.
     * GET|HEAD /bookings/{id}
     *
     * @param int $id
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/bookings/{id}",
     *      summary="Display the specified Bookings",
     *      tags={"Bookings"},
     *      description="Get Bookings",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bookings",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bookings"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Bookings $bookings */
        $bookings = $this->bookingsRepository->find($id)
            ->with([
                'services' => function(BelongsToMany $query) {
                    $query
                        ->select(['id', 'name', 'cost', 'duration', 'app_id']);
                },
                'startSchedule' => function(BelongsTo $query) {
                    $query
                        ->select(['id', 'datetime_from', 'datetime_to']);
                }])
            ->get();

        if (empty($bookings)) {
            return $this->sendError('Bookings not found');
        }

        return $this->sendResponse($bookings->toArray(), 'Bookings retrieved successfully');
    }

    /**
     * Remove the specified Bookings from storage.
     * DELETE /bookings/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return JsonResponse
     *
     * @SWG\Delete(
     *      path="/bookings/{id}",
     *      summary="Remove the specified Bookings from storage",
     *      tags={"Bookings"},
     *      description="Delete Bookings",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bookings",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Bookings $bookings */
        $bookings = $this->bookingsRepository->find($id);

        if (empty($bookings)) {
            return $this->sendError('Bookings not found');
        }

        $bookings->delete();

        return $this->sendSuccess('Bookings deleted successfully');
    }
}
