<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSchedulesAPIRequest;
use App\Http\Requests\API\UpdateSchedulesAPIRequest;
use App\Models\Schedules;
use App\Repositories\SchedulesRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Swagger\Annotations as SWG;

/**
 * Class SchedulesController
 * @package App\Http\Controllers\API
 */

class SchedulesAPIController extends AppBaseController
{
    /** @var  SchedulesRepository */
    private $schedulesRepository;

    public function __construct(SchedulesRepository $schedulesRepo)
    {
        $this->schedulesRepository = $schedulesRepo;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/schedules",
     *      summary="Get a listing of the user Schedules.",
     *      tags={"Schedules"},
     *      description="Get user all Schedules",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="user_id",
     *          description="id of Schedules owner",
     *          type="integer",
     *          required=true,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Schedules")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $schedules = $this->schedulesRepository->all(
            ['user_id' => $request->get('user_id')] + $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($schedules->toArray(), 'Schedules retrieved successfully');
    }

    /**
     * @param CreateSchedulesAPIRequest $request
     * @return JsonResponse
     *
     * @SWG\Post(
     *      path="/schedules",
     *      summary="Store a newly created Schedules in storage",
     *      tags={"Schedules"},
     *      description="Store Schedules",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Schedules that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Schedules")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Schedules"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSchedulesAPIRequest $request)
    {
        $input = $request->all();

        $schedules = $this->schedulesRepository->create($input);

        return $this->sendResponse($schedules->toArray(), 'Schedules saved successfully');
    }

    /**
     * @param int $id
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/schedules/{id}",
     *      summary="Display the specified Schedules",
     *      tags={"Schedules"},
     *      description="Get Schedules",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Schedules",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Schedules"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Schedules $schedules */
        $schedules = $this->schedulesRepository->find($id);

        if (empty($schedules)) {
            return $this->sendError('Schedules not found');
        }

        return $this->sendResponse($schedules->toArray(), 'Schedules retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSchedulesAPIRequest $request
     * @return JsonResponse
     *
     * @SWG\Put(
     *      path="/schedules/{id}",
     *      summary="Update the specified Schedules in storage",
     *      tags={"Schedules"},
     *      description="Update Schedules",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Schedules",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Schedules that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Schedules")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Schedules"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSchedulesAPIRequest $request)
    {
        $input = $request->all();

        /** @var Schedules $schedules */
        $schedules = $this->schedulesRepository->find($id);

        if (empty($schedules)) {
            return $this->sendError('Schedules not found');
        }

        $schedules = $this->schedulesRepository->update($input, $id);

        return $this->sendResponse($schedules->toArray(), 'Schedules updated successfully');
    }

    /**
     * Remove the specified schedules from storage.
     * DELETE /schedules/{id}
     *
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return JsonResponse
     *
     * @SWG\Delete(
     *      path="/schedules/{id}",
     *      summary="Remove the specified Schedules from storage",
     *      tags={"Schedules"},
     *      description="Delete Schedules",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Schedules",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Schedules $schedules */
        $schedules = $this->schedulesRepository->find($id);

        if (empty($schedules)) {
            return $this->sendError('Schedules not found');
        }

        $schedules->delete();

        return $this->sendSuccess('Schedules deleted successfully');
    }
}
