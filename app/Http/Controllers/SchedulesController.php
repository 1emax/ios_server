<?php


namespace App\Http\Controllers;


use App\Models\Schedules;
use App\Models\Services;
use Carbon\Carbon;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class SchedulesController extends Controller
{

    public function userFreeTime($userId, $date, $service_ids = '')
    {
        $query = Schedules::where(new Expression('DATE(schedules.datetime_from)'), '=', $date);
        $duration = null;

        if (!empty($service_ids)) {
            $servicesDuration = Services::select(new Expression('SUM(duration) as duration'))
                ->where('user_id', $userId)
                ->whereIn('id', explode('|', $service_ids))
                ->groupBy('user_id')
                ->first()
                ->toArray();

            $duration = !empty($servicesDuration) &&  !empty($servicesDuration['duration']) ? $servicesDuration['duration'] : null;

            if ($duration > 0) {
                $query
                    ->leftJoin('schedules as s2', function(JoinClause $join) use ($duration) {
                        $join
                            ->on(new Expression('DATE_ADD(schedules.datetime_from, INTERVAL '.$duration.' MINUTE)'), '>',   's2.datetime_from')
                            ->on('schedules.datetime_from', '<', 's2.datetime_from')
                            ->on('s2.user_id', '=', 'schedules.user_id')
                            ->on(new Expression('DATE(s2.datetime_from)'), '=', new Expression('DATE(schedules.datetime_from)'))
                            ->where('s2.reserved', '=', '1');
                    })
                    ->select([new Expression('MAX(s2.reserved) as disable')]);
            }
        }

        return \Response::json(
            [
                'times' => $query
                    ->where('schedules.user_id', '=', $userId)
                    ->where('schedules.datetime_to', '>' , Carbon::now())
                    ->where('schedules.reserved', false)
                    ->addSelect([new  Expression("DATE_FORMAT(schedules.datetime_from, '%H:%i') as time"), 'schedules.id'])
                    ->groupBy('schedules.id')
                    ->get(),
                'duration' => $duration
            ]
        );
    }
}
