@php /** @var $user \App\Models\Users */ @endphp
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimum-scale=1, shrink-to-fit=no">

    <title>{{ $user->name }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="/css/select2.min.css">
{{--    <script src="/js/bootstrap-datetimepicker.min.js"></script>--}}
</head>

<body{{-- style="position: relative; overflow-x: hidden;"--}}>
    <div class="container">
        <h1 class="text-center">{{ $user->name }}</h1>
        {{ Form::model($user, ['method' => 'post', 'route' => 'new_book', 'lang' => 'ru']) }}
            {{ Form::hidden('id', null, ['id' => 'user-id', 'name' => 'user_id']) }}
            <div class="row flex-row form-group">
                <div class="col-md-12">
                     <h2>Услуги</h2>
                </div>
                <div class="col-md-12">
                    <select id="services" name="service_ids[]" multiple="multiple" class="form-control" required="required">
                        @foreach($user->services as $service)
                            <option value="{{ $service->id }}">{{ $service->name }} ({{ $service->cost }} р.)</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row flex-row">
                <div class="col-md-12">
                    <h2>Дата</h2>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4" required="required" style="display: none;"/>
                            {{--<div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>--}}
                            <script>
                                var datePickerAllowedDates = {!! @json_encode($user->schedules->pluck('schedule_date')) !!};
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row flex-row form-group">
                <div class="col-md-12">
                    <h2>Выберите время</h2>
                </div>
                <div class="col-md-12">
                    <select id="select-time" name="start_schedule_id" class="form-control" required="required"></select>
                </div>
            </div>
            <div class="row flex-row form-group">
                <div class="col-md-12">
                    <h3>Укажите Ваш номер телефона</h3>
                </div>
                <div class="col-md-12">
                    <input type="tel" required="required" name="phone" class="form-control">
                </div>
            </div>
        <div class="row flex-row form-group">
            <div class="col-md-12">
                <h3>Ваше имя</h3>
            </div>
            <div class="col-md-12">
                <input type="text" required="required" name="name" class="form-control">
            </div>
        </div>
            <div class="row flex-row form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-info pull-right">Записаться</button>
                </div>
            </div>
        {{ Form::close() }}
    </div>

</body>
</html>
