@php /** @var $booking \App\Models\Bookings */ @endphp
    <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimum-scale=1, shrink-to-fit=no">

    <title>Оформлена запись на {{ $booking->startSchedule->datetime_from }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css">
    {{--    <script src="/js/bootstrap-datetimepicker.min.js"></script>--}}
</head>

    <body>
        <div class="container">
            <h2>Запись оформлена!</h2>
            <h3>Время: {{ $booking->startSchedule->datetime_from }}</h3>
            <h3>Услуги</h3>
            <ul>
                @foreach($booking->services as $service)
                    <li>{{ $service->name }}</li>
                @endforeach
            </ul>
        </div>
    </body>
</html>
