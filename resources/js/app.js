require('./bootstrap');
// Register $ global var for jQuery
import $ from 'jquery';
window.$ = window.jQuery = $;
// Import jQuery Plugins
import './bootstrap-datetimepicker.min.js';

$(function () {
    $('#datetimepicker4').datetimepicker({
        format: 'YYYY-MM-DD',
        enabledDates: datePickerAllowedDates,
        minDate: new Date(),
        inline: true,
        showClose:false,
        keepOpen:true,
        locale: 'ru'
    });

    $("#datetimepicker4").on("dp.change", function (e) {
        var userId = $("#user-id").val();
        var services = $("#services").val();

        var $times = $("#select-time").empty();

        var filterByServices = "";

        if (services.length > 0) {
            filterByServices = '/' +  services.join('|')
        }

        $.get('/free_time/' + userId + '/' + e.date.format('YYYY-MM-DD') + filterByServices, function (resp) {
            console.log(resp);
            if (resp && resp.times && resp.times.length) {
                $.each(resp.times, function() {
                    $times.append($("<option />").prop("disabled", this.disable).val(this.id).text(this.time));
                });
            }
        }, "json")
    });

    $("#services").select2({
        placeholder: "Выберите услуги",
    });
    $("#select-time").select2({
        placeholder: "Выберите время",
    });

    $('#services').on('select2:select', function (e) {
        var data = e.params.data;
        console.log(data);

        var datePickerVal = $("#datetimepicker4 > input").val();
        if (datePickerVal == '') return;

        var userId = $("#user-id").val();

        var $times = $("#select-time").empty();

        $.get('/free_time/' + userId + '/' + datePickerVal + "/" + $(this).val().join('|'), function (resp) {
            console.log(resp);

            if (resp && resp.times && resp.times.length) {
                $.each(resp.times, function() {
                    $times.append($("<option />").prop("disabled", this.disable).val(this.id).text(this.time));
                });
            }
        }, "json")
    });
});
