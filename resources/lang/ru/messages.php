<?php

return [
  'This password reset link will expire in :count minutes.' => 'Ссылка сброса пароля будет активна в течение :count минут'
];
